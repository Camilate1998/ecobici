<?php 
	include("../../settings/settings.php");
	include("../../backend/querys.php");
	$idcone=conexion();
	$ObtenerCliente=ObtenerCliente();
	$sql_a=mysqli_query($idcone, $ObtenerCliente);
?>
	
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport"  http-equiv="Content-Type" content="width=100%; initial-scale=1; maximum-scale=1; minimum-scale=1; user-scalable=no;text/html; charset=UTF-8" />
	<title>Prueba</title>
	<!-- <link href="static_files/bootstrap/css/bootstrap.min.css" rel="stylesheet"> -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
	<div class="jumbotron text-center" style="margin-bottom:0">
	  <h1>EcoBici</h1>
	  <p>La solución esta en nuestras manos..</p> 
	</div>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
		<div class="collapse navbar-collapse" id="navbarNavDropdown">
			<ul class="navbar-nav">
				<li class="nav-item active">
					<a class="nav-link" href="../../index.php">Home <span class="sr-only">(current)</span></a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="listview.php">Clientes</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="../bicicletas/listview.php">Bicicletas</a>
				</li>
			</ul>
		</div>
	</nav>
  <div class="container" style="margin-top:30px">
		<p>
			<a class="btn btn-primary" href="createview.php">Crear Cliente</a>
		</p>
		<p>Lista de clientes</p>		
		<div class="row">
			<div class="col-sm-6">
				<table class="table">
				    <thead>
				      <tr>
				        <th>Nombres</th>
				        <th>Apellidos</th>
				        <th>Cédula</th>
				        <th>Dirección</th>
				        <th>Teléfono</th>
				        <th>Celular</th>
				        <th>Fecha de nacimiento</th>
				      </tr>
				    </thead>
				    <tbody>
				    <?php
						while($fila=mysqli_fetch_array($sql_a)){
					?>
				      <tr>
				        <td><?php echo $fila ['nombres'] ?></td>
				        <td><?php echo $fila ['apellidos'] ?></td>
				        <td><?php echo $fila ['cedula'] ?></td>
				        <td><?php echo $fila ['direccion'] ?></td>
				        <td><?php echo $fila ['telefono'] ?></td>
				        <td><?php echo $fila ['celular'] ?></td>
				        <td><?php echo $fila ['fecha_nacimiento'] ?></td>
				        <td>
				        	<a 
				        	href='createview.php?cedula=<?php echo $fila ['cedula'] ?>'>
				        	<img title="Modificar Cliente" src="../../static_files/img/edit.png" width="16px" height="16px"></a>
				        </td>
				      </tr>
				    <?php 
						} 
					?>
				    </tbody>
				 </table>
			</div>
		</div>
	</div>
</body>
<!-- <script src="static_files/jquery/jquery-1.11.1.min.js"></script>
<script src="static_files/bootstrap/js/bootstrap.min.js"></script> -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</html>