<?php 
	include("../../settings/settings.php");
	include("../../backend/querys.php");
	$idcone=conexion();

    $nombres="";
	$apellidos="";
	$cedula="";
	$direccion="";
	$telefono="";
	$celular="";
	$fecha_nacimiento="";
	$guardar = 0;
	
	if (!empty($_REQUEST['cedula'])){
		$ObtenerClienteCedula=ObtenerClienteCedula($_REQUEST['cedula']);
		$sql_c=mysqli_query($idcone, $ObtenerClienteCedula);
		$fila=mysqli_fetch_array($sql_c);
		// echo "<pre>".print_r($fila,true)."</pre>";

		$nombres .= "disabled='true' value ='".$fila['nombres']."'";
		$apellidos .= "disabled='true' value ='".$fila['apellidos']."'";
		$cedula .= "readonly='true' value ='".$fila['cedula']."'";
		
		$direccion .= "value ='".$fila['direccion']."'";
		$telefono .= "value ='".$fila['telefono']."'";
		$celular .= "value ='".$fila['celular']."'";
		$fecha_nacimiento .= "value ='".$fila['fecha_nacimiento']."'";
		$guardar = 1;
	}
	
?>
	
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport"  http-equiv="Content-Type" content="width=100%; initial-scale=1; maximum-scale=1; minimum-scale=1; user-scalable=no;text/html; charset=UTF-8" />
	<title>Prueba</title>
	<!-- <link href="static_files/bootstrap/css/bootstrap.min.css" rel="stylesheet"> -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
	<div class="jumbotron text-center" style="margin-bottom:0">
	  <h1>EcoBici</h1>
	  <p>La solución esta en nuestras manos..</p> 
	</div>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<div class="collapse navbar-collapse" id="navbarNavDropdown">
			<ul class="navbar-nav">
				<li class="nav-item active">
					<a class="nav-link" href="../../index.php">Home <span class="sr-only">(current)</span></a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="listview.php">Clientes</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="../bicicletas/listview.php">Bicicletas</a>
				</li>
			</ul>
		</div>
	</nav>
	<div class="container" style="margin-top:30px">
		<p>Formulario de crear clientes</p>	
  		<div class="row">
	    	<div class="col-sm-6">
				<form  action="../../backend/ecobici.php" method="POST">
					<input type="number" name="guardar" value='<?php echo $guardar ?>' hidden="true">
					<div class="form-group">
						<label for="nombres">Nombres</label>
						<input type="text" class="form-control" id="nombres" name="nombres" placeholder="nombres" <?php echo $nombres ?> >
					</div>
					<div class="form-group">
						<label for="apellidos">Apellidos</label>
						<input type="text" class="form-control" id="apellidos" name="apellidos" placeholder="Apellidos" <?php echo $apellidos ?>>
					</div>
					<div class="form-group">
						<label for="cedula">Cédula</label>
						<input type="text" class="form-control" id="cedula" name="cedula" placeholder="Cédula" <?php echo $cedula ?>>
					</div>

					<div class="form-group">
						<label for="direccion">Dirección</label>
						<input type="text" class="form-control" id="direccion" name="direccion" placeholder="Dirección" <?php echo $direccion ?>>
					</div>

					<div class="form-group">
						<label for="telefono">Teléfono</label>
						<input type="text" class="form-control" id="telefono" name="telefono" placeholder="Teléfono" <?php echo $telefono ?>>
					</div>

					<div class="form-group">
						<label for="celular">Celular</label>
						<input type="text" class="form-control" id="celular" name="celular" placeholder="Celular" <?php echo $celular ?>>
					</div>

					<div class="form-group">
						<label for="fecha_nacimiento">Fecha de nacimiento</label>
						<input type="date" class="form-control" id="fecha_nacimiento" name="fecha_nacimiento" placeholder="Fecha de nacimiento" <?php echo $fecha_nacimiento ?>>
					</div>
					<button type="submit" class="btn btn-primary">Guardar</button>
				</form>
			</div>
		</div>
	</div>
</body>
<!-- <script src="static_files/jquery/jquery-1.11.1.min.js"></script>
<script src="static_files/bootstrap/js/bootstrap.min.js"></script> -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</html>